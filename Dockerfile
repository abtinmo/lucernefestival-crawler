FROM python:3.11-slim-buster

# set work directory
WORKDIR /usr/

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements/base.txt .
RUN pip install --no-cache-dir -r base.txt

# copy project
COPY ./src /usr/src/

ENV PYTHONPATH "${PYTHONPATH}:$APP_HOME"
