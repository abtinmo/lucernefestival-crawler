from unittest.mock import patch

import pytest

from httpx import Response, TimeoutException

from src.main import get_web_page_content


@pytest.mark.asyncio
async def test_request_retry_is_aborted_after_rate_limit():
    async def side_effect(*args, **kwargs):
        return Response(text="", status_code=429)

    with patch("httpx._client.AsyncClient.request", side_effect=side_effect) as mock_object:
        await get_web_page_content(url="https://example.de", retries=3)
    assert mock_object.call_count == 1


@pytest.mark.asyncio
async def test_request_retry_is_retired_on_time_out():
    with patch("httpx._client.AsyncClient.request", side_effect=TimeoutException(message="timeout")) as mock_object:
        await get_web_page_content(url="https://example.de", retries=3)
    assert mock_object.call_count == 3


@pytest.mark.asyncio
async def test_request_is_not_executed_on_negative_number():
    with patch("httpx._client.AsyncClient.request") as mock_object:
        await get_web_page_content(url="https://example.de", retries=-1)
    assert mock_object.call_count == 0
