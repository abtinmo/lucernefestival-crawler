from os import environ


TORTOISE_ORM_CONFIG = {
    "connections": {"default": environ.get("DB_URL")},
    "apps": {
        "models": {
            "models": ["aerich.models", "src.models"],
            "default_connection": "default",
        },
    },
}
