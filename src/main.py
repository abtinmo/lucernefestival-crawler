import asyncio
import datetime
import logging

from functools import cached_property
from os import environ
from typing import Iterable, Union

import httpx
import matplotlib.pyplot as plt

from bs4 import BeautifulSoup
from tortoise import Tortoise
from tortoise.functions import Count

from src.config import TORTOISE_ORM_CONFIG
from src.models import Event


class LucerneFestival:

    def __init__(self, content: str) -> None:
        self.soup = BeautifulSoup(content, "html.parser")
        self.base_url = environ.get("BASE_URL")

    @staticmethod
    def clean_up_text(text: str) -> str:
        return text.replace("\n", "").replace("\t", "").strip()

    @property
    def events(self) -> Iterable[str]:
        for program in self.soup.findAll("p", class_="event-title h3"):
            yield self.base_url + program.find("a", href=True)["href"]

    @property
    def date(self) -> datetime.datetime:
        return datetime.datetime.strptime(self.soup.title.text[:10], "%d.%m.%Y")

    @cached_property
    def _time(self) -> str:
        time = list(self.soup.find("div", class_="cell large-6 subtitle").children)[-1].text.split("|")[1].strip()
        return self.clean_up_text(time).replace(".", ":")

    @classmethod
    def is_duration(cls, time: str) -> bool:
        return "/" in time

    @property
    def time(self):
        time = self._time
        if self.is_duration(time):
            return time.split("/")[0].strip()
        return time

    @property
    def duration(self) -> datetime.timedelta | None:
        time = self._time
        if not self.is_duration(time):
            return None
        start, end = time.split("/")
        return datetime.datetime.strptime(end.strip(), "%H:%M") - datetime.datetime.strptime(start.strip(), "%H:%M")

    @property
    def picture(self) -> str:
        return self.base_url + self.soup.find("figure").find("img").get("src")

    @property
    def address(self) -> str | None:
        address_section = self.soup.find("div", class_="cell medium-9").findAll("p")
        if not address_section:
            return None
        return address_section[-1].text

    @property
    def description(self) -> str:
        section = self.soup.find("section", {"id": "description"})
        return section.find("div", class_="readmore-text").text.strip() if section else ""

    @property
    def title(self) -> str:
        return self.soup.find(
            "header", class_="page-header clip-path clr-pri",
        ).find(
            "div", class_="grid-x grid-margin-x align-center",
        ).find("h1").text

    @property
    def artists(self) -> list[str]:
        artists = self.soup.find("ul", {"class": "performers-list grid-x grid-margin-x"})
        if not artists:
            return []
        return [artist.text.strip() for artist in artists.find_all("a")]

    @property
    def event_data(self) -> dict[
        str,
        Union[str, None, datetime.datetime, datetime.timedelta, list[str]],
    ]:
        return {
            "address": self.address,
            "artists": self.artists,
            "title": self.title,
            "date": self.date,
            "time": self.time,
            "picture": self.picture,
            "description": self.description,
            "duration": self.duration,
        }


async def get_web_page_content(url: str, retries=3) -> str | None:
    """
    Retry the request if faced any timeouts our unexpected status codes.
    In case of 403 (Rate limiting), break the  operation.
    """
    if retries <= 0:
        return None
    retries -= 1
    async with httpx.AsyncClient() as ac:
        try:
            response = await ac.get(url, timeout=int(environ.get("TIMEOUT", 10)))
        except httpx.TimeoutException as ex:
            logging.error(f"timeout during calling {url}", exc_info=ex)
            return await get_web_page_content(url=url, retries=retries)
    if response.status_code == 200:
        return response.text
    if response.status_code == 429:
        return None
    return await get_web_page_content(url=url, retries=retries)


async def fetch_events() -> list[dict]:
    main_page = await get_web_page_content(environ.get("EVENT_PAGE", ""))
    if not main_page:
        raise SystemExit()
    event_links = LucerneFestival(content=main_page).events
    event_contents = await asyncio.gather(
        *[get_web_page_content(program_link) for program_link in event_links],
    )
    events: list[dict] = []
    for content in event_contents:
        if content:
            events.append(LucerneFestival(content).event_data)
    return events


async def create_database_records(event_data: list[dict]) -> None:
    await Tortoise.init(config=TORTOISE_ORM_CONFIG)
    await Tortoise.generate_schemas(safe=True)
    await Event.bulk_create(Event(**event) for event in event_data)


async def get_events_per_day() -> list[dict[str, Union[str, int]]]:
    return await Event.annotate(
        count=Count("id"),
    ).group_by("date").order_by("date").values("date", "count")


def create_plot(records: list[dict[str, Union[str, int]]]):
    x_points, y_points = [], []
    for record in records:
        x_points.append(record["date"])
        y_points.append(record["count"])
    # change figure size to fit the data cleanly.
    plt.rcParams["figure.figsize"] = (20, 10)
    plt.bar(x_points, y_points)
    plt.xlabel("dates")
    plt.ylabel("events")
    plt.savefig(f"src/asset/{datetime.datetime.now()}.png", dpi=100)


async def main() -> None:
    event_data = await fetch_events()
    await create_database_records(event_data)
    records = await get_events_per_day()
    create_plot(records)

if __name__ == "__main__":
    asyncio.run(main())
