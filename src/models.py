import uuid

from tortoise import fields
from tortoise.contrib.postgres.fields import ArrayField
from tortoise.models import Model


class Event(Model):
    id = fields.UUIDField(default=uuid.uuid4, pk=True)
    title = fields.TextField()
    address = fields.TextField(null=True)
    date = fields.DateField()
    time = fields.TimeField()
    artists = ArrayField(element_type=fields.TextField.SQL_TYPE)
    picture = fields.TextField()
    description = fields.TextField()
    duration = fields.TimeDeltaField(null=True)
