A script that crawls events from lucernefestival, inserts data into a database and at the end creates a plot which shows the total events of each day.


# Getting started


make a copy from the sample env file and change the variables if needed.

    cp .env.sample .env

run the project with docker 

    docker compose up 

check the asset directory to see the result.


# code quality

to run the code check, install and run tox

    pip install tox  
    
    tox
